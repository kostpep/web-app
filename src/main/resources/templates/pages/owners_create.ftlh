<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Owner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/dashboard.css">
</head>
<body>

<#include "../partials/navbar.ftlh">

<div class="container-fluid">
    <div class="row">
        <#include "../partials/sidebar_owners_create_search.ftlh">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="row">
                <div class="col-md-12 order-md-1">
                    <h4 class="mb-3">Create Owner</h4>
                    <form id="createForm" action="/admin/owners/create" method="post">

                        <div class="mb-3">
                            <label for="social-number">Tax Registry Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="social-number" name="taxRegistryNumber" required>
                                <div class="invalid-feedback" style="width: 100%;">
                                    Social number is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" required>
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" required>
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address" required>
                            <div class="invalid-feedback">
                                Please enter an address.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                            <div class="invalid-feedback">
                                Please enter a valid email address.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="phoneNumber">Phone number</label>
                            <input type="phoneNumber" class="form-control" id="phoneNumber" name="phoneNumber" required>
                            <div class="invalid-feedback">
                                Please enter a valid phone number.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                            <div class="invalid-feedback">
                                Please enter a valid password.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="type">Property type</label>
                            <select class="custom-select d-block w-100" id="type" name="type" required>
                               <#if ownersType??>
                                <#list ownersType as type>
                                <option value="${type}">${type}</option>
                                </#list>
                                </#if>
                            </select>
                            <div class="invalid-feedback">
                                Please select a property type.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="role">Owner's role</label>
                            <select class="custom-select d-block w-100" id="role" name ="role" required>
                                <#if ownersRole??>
                                    <#list ownersRole as role>
                                        <option value="${role}">${role}</option>
                                    </#list>
                                </#if>
                            </select>
                            <div class="invalid-feedback">
                                Please select an owner's role.
                            </div>
                        </div>

                        <button class="btn btn-primary btn-lg mt-4" type="submit">Create</button>
                    </form>
                </div>
            </div>
        </main>
    </div>
</div>

<#include "../partials/scripts.ftlh">
<script src="./../js/validations.js"></script>
</body>
</html>
