package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.forms.OwnerForm;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("admin")
public class OwnerSearchController {
    private static final String OWNER_LIST = "owners";
    private static final String Owner_FORM = "ownersForm";
    @Autowired
    private OwnerService ownerService;

    @GetMapping(value = "/ownersSearch")
    public String ownersSearch(Model modelMap) {
        modelMap.addAttribute("searchForm", new OwnerForm());
        return "SearchOwner";
    }

    @PostMapping("/ownersSearch")
    public String ownersSearch(@ModelAttribute("SearchOwner") OwnerForm searchOwner, ModelMap modelMap) {
        String taxRegistryNumber;
        String email = searchOwner.getEmail();
        List<OwnerModel> owners;
        try {
        if (email.isEmpty() && !searchOwner.getTaxRegistryNumber().isEmpty()) {
            taxRegistryNumber = searchOwner.getTaxRegistryNumber(); //if email field is empty, we search by taxRegistryNumber
            owners = ownerService.findOwnerByTaxRegistryNumber(Long.parseLong(taxRegistryNumber));
            if (!owners.isEmpty()) {
                modelMap.addAttribute(OWNER_LIST, owners);
            }
        }
        if (!email.equals(null) && searchOwner.getTaxRegistryNumber().isEmpty()) {
            owners = ownerService.findOwnerByEmail(email);
            if (!owners.isEmpty()) {
                modelMap.addAttribute(OWNER_LIST, owners);
            }
        }
        if (email.isEmpty() && searchOwner.getTaxRegistryNumber().toString().isEmpty()) {
            return "SearchOwner";
        } else if (!email.isEmpty() && !searchOwner.getTaxRegistryNumber().toString().isEmpty()) {
            return "SearchOwner";
        }
        }catch (NumberFormatException ex)
        {
            return "SearchOwner";
        }
        return "owners";
    }
}