package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("admin")
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @GetMapping(value = "/owners")
    public String owners(Model model) {
        List<OwnerModel> owners=ownerService.findAll();
        model.addAttribute("owners", owners);
        return "owners";
    }
}
