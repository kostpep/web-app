package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("admin")
public class RepairController {

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/repairs")
    public String repairs(Model model) {
        List<RepairModel> repairs=repairService.findAll();
        model.addAttribute("repairs", repairs);
        return "repairs";
    }
}
